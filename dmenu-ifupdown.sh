#!/bin/sh

DMENU_PROGRAM=dmenu
if [ -n "$WAYLAND_DISPLAY" ]; then
    DMENU_PROGRAM="tofi --prompt-text network":
fi

INTERFACES_LIST_FILE=~/.local/available_interfaces

running_intefeaces_regex="^($(/sbin/ifquery --state | cut -d = -f 1 | awk 'NR==1{regex="" $0;} NR>1{regex=regex"|"$0;} END{print regex;}'))"

dmenu_selection=$({
    /sbin/ifquery --state | awk -F= '{print  "down:" $1;}'
    grep -vE ${running_intefeaces_regex} ${INTERFACES_LIST_FILE}
} | $DMENU_PROGRAM)

awesome_notify () {
    if [ $1 -eq 0 ]; then
	notify-send "$3 $2 succeded"
    else
	notify-send "$3 $2 failed"
EOF
    fi
}

case $dmenu_selection in
    down:*) sudo ifdown $(echo $dmenu_selection| cut -d: -f2); res=$?; awesome_notify $res DOWN $(echo $dmenu_selection| cut -d: -f2);;
    *) sudo ifup $dmenu_selection ; res=$?; awesome_notify $res UP $dmenu_selection;;
esac
	    

