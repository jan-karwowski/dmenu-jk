#!/bin/sh
 # change pulse card profile

DMENU_PROGRAM=dmenu
if [ -n "$WAYLAND_DISPLAY" ]; then
    DMENU_PROGRAM="tofi --prompt-text pa:"
fi



CARDIDX=$(pacmd list-cards | awk '/    index: /{match($0, /([0-9]+)/, arr); cardIdx=arr[1]} /\t\tdevice.description =/{match($0, /device\.description = "(.*)"/, arr); print cardIdx ":" arr[1]}' | $DMENU_PROGRAM | cut -d: -f1)

if [ -z "$CARDIDX" ] ; then
    true
else
    PAPROFILE=$(pacmd list-cards | awk '/    index: /{match($0, /([0-9]+)/, arr); if('$CARDIDX'==arr[1]) {idxmatch=1;} else {idxmatch=0;}} /^\tprofiles:/{profilesmatch=1} /^\tactive profile:/{profilesmatch=0} /\t\t/{if(profilesmatch && idxmatch){match($0, /([-_a-z0-9:]+): /, arr); print arr[1];}}' | $DMENU_PROGRAM)
    if [ ! -z "$PAPROFILE" ] ; then
	pacmd set-card-profile $CARDIDX $PAPROFILE
    fi
fi
