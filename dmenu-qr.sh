#!/bin/sh

DMENU_PROGRAM=dmenu
IMV=imv-x11
if [ -n "$WAYLAND_DISPLAY" ]; then
    DMENU_PROGRAM="tofi --prompt-text qr-display:"
    IMV=imv-wayland
fi



dmenu_res=$(
    $DMENU_PROGRAM <<EOF
primary
secondary
clipboard
EOF
	 )
if [ -n "$WAYLAND_DISPLAY" ]; then
    case $dmenu_res in
	primary) exec qrcode -s 1024  $(wl-paste --primary) | $IMV - ;;
	clipboard) exec qrcode -s 1024  $(wl-paste) | $IMV - ;;
	*) exit 1;;
    esac
else
    case $dmenu_res in
	primary) exec qrcode -s 1024  $(xsel --primary) | $IMV - ;;
	secondary) exec qrcode -s 1024  $(xsel --secondary) | $IMV - ;;
	clipboard) exec qrcode -s 1024  $(xsel --clipboard) | $IMV - ;;
	*) exit 1;;
    esac
fi
    
