#!/bin/sh

DMENU_PROGRAM=dmenu
if [ -n "$WAYLAND_DISPLAY" ]; then
    DMENU_PROGRAM="tofi --prompt-text utility":
fi



dmenu_res=$(
    $DMENU_PROGRAM <<EOF
network
pulse
qr
EOF
	 )

case $dmenu_res in
    network) exec dmenu-ifupdown.sh ;;
    pulse) exec dmenu-pulse.sh ;;
    qr) exec dmenu-qr.sh ;;
    *) exit 1;;
esac
